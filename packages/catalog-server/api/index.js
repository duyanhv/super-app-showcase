const express = require('express');
const {exec} = require('child_process');

// if running on vercel, use generated .prod configs
const env = process.env.VERCEL ? '.prod' : '.dev';
const suffix = env + '.json';

const host = require('../data/host' + suffix);
const shell = require('../data/host' + suffix);
const booking = require('../data/booking' + suffix);
const shopping = require('../data/shopping' + suffix);
const dashboard = require('../data/dashboard' + suffix);

const app = express();
const port = process.env.PORT ?? 3000;

app.get('/host', (req, res) => {
  const platform = req.query.platform;
  const appVersion = req.query.appVersion;

  res.send(host[platform][appVersion]);
});
app.get('/shell', (req, res) => {
  const platform = req.query.platform;
  const appVersion = req.query.appVersion;

  res.send(shell[platform][appVersion]);
});
app.get('/booking', (req, res) => {
  const platform = req.query.platform;
  const appVersion = req.query.appVersion;

  res.send(booking[platform][appVersion]);
});
app.get('/shopping', (req, res) => {
  const platform = req.query.platform;
  const appVersion = req.query.appVersion;

  res.send(shopping[platform][appVersion]);
});
app.get('/dashboard', (req, res) => {
  const platform = req.query.platform;
  const appVersion = req.query.appVersion;

  res.send(dashboard[platform][appVersion]);
});

app.post('/build-bundle', (req, res) => {
  // Handle the POST request here
  const {projectId, appVersion, secretKey} = req.body;
  // {
  //   projectId: '1234',
  //   appVersion: '0.1.0',
  //   secretKey: 'asdf',
  // }
  if (!secretKey || secretKey !== process.env.SECRET_KEY) {
    res.status(401).send('Unauthorized');
    return;
  }
  const command = `PROJECT_ID=${projectId} APP_VERSION=${appVersion} node ../scripts/build.js`;
  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      res.status(500).send('Error running build command');
      return;
    }

    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
    res.send('Build completed successfully');
  });
  console.log(user);
  res.send('User created successfully');
});

app.listen(port, () => {
  console.log(`[CatalogServer] Server listening at port ${port} `);
});

module.exports = app;
